# Word Count tool (`wc`)

The `wc` (word count) tool counts and prints the number of lines, words, and
characters (bytes) present in a text file.  

While you and I would consider punctuation characters to separate words, the
classic `wc` program doesn't.  It considers only white space characters to be
word boundaries.

For example, you will recognize four words below, but `wc` considers this to be
a single word:

    Julianna,17,Snow,wriggle

To be correct, your version of `wc` must likewise consider this to be one word.
For comparison, here is what the classic Unix version of `wc` outputs when
given the file `data/people.csv` as input compared to my solution in Python:

    $ wc data/people.csv
    9  18 278 data/people.csv

    $ python sln/tt.py wc data/people.csv
    9   18  278 data/people.csv

If you look closely you'll notice that my program's output differs from `wc`'s
in the amount of white space.  It is also okay for your program's output to
differ in this inconsequential way.  Your program must, however, agree in the
essential facts.

The meanings of the numbers are, from left to right

*   lines
*   words
*   characters (or bytes)


To further illustrate how `wc` perceives word boundaries, I added `print()`
statements to my solution to show you how it arrived at its word count.

    $ python sln/tt.py wc data/people.csv
    len(['Name,Age,Favorite', 'Color,Locomotion', 'Style']) = 3
    len(['Adrianna,22,Royal', 'Blue,crawl']) = 2
    len(['Julian,36,Midnight', 'Blue,traipse']) = 2
    len(['Tiffany,24,Light', 'Salmon,push']) = 2
    len(['Savannah,39,Antique', 'White,march']) = 2
    len(['Abraham,26,DarkSea', 'Green,trot']) = 2
    len(['Michael,23,Dodger', 'Blue,lurch']) = 2
    len(['Marcus,29,Dark', 'Goldenrod,slink']) = 2
    len(['Julianna,17,Snow,wriggle']) = 1
    9   18  278 data/people.csv


Here are some more examples:

    $ python src/tt.py wc data/num2
    2	2	4	data/num2


    $ python src/tt.py wc data/words200
    200	200	1790	data/words200


Multiple files may be given at once:

    $ python src/tt.py wc data/let3 data/random20 data/people.csv data/dup5 
    3	3	6	data/let3
    20	20	51	data/random20
    9	18	278	data/people.csv
    8	8	16	data/dup5


The program aborts as soon as a non-existent file is encountered.

    $ python src/tt.py wc data/let3 data/random20 data/DOES_NOT_EXIST data/people.csv data/dup5 
    3	3	6	data/let3
    20	20	51	data/random20
    Traceback (most recent call last):
      File "src/tt.py", line 74, in <module>
        ops[sys.argv[1]](sys.argv[2:])
      File "/home/fadein/school/Sp19/cs1440/Assn/2/src/WordCount.py", line 5, in wc
        f = open(file)
    FileNotFoundError: [Errno 2] No such file or directory: 'data/DOES_NOT_EXIST'


Be aware that, due to differences in the representation of the end-of-line
(EOL) sequence between operating systems, the byte count you see may vary from
my examples.  On Linux and Mac, '\n' (ASCII byte 10) is used to separate lines
of files.  Windows uses a two-byte sequence '\r\n' (ASCII byte 13 followed by
byte 10) for this purpose.

These examples were produced on my Linux laptop.  If you are a Windows user
your byte count may be greater than mine, though the line and word counts
should remain the same.
