def cut(args):
    """remove sections from each line of files"""
    file_arg_spot = 2
    removal_index = []
    if args[0] == "-f":
        if len(args) < 3:
            print("Sorry bud, not enough arguments")
        else:
            file_arg_spot = 2
            remove = args[1].split(',')
            for i in range(0, len(remove)):
                removal_index.append(int(remove[i]) - 1)
            removal_index.sort()
    else:
        removal_index.append(0)
    file = open(args[file_arg_spot], 'r')
    file_read = file.readlines()
    file.close()
    split_read = []
    after_cut = []
    for i in range(0, len(file_read)):
        split_read.append(file_read[i].split(','))
    for i in range(0, len(split_read)):
        after_cut.append([])
        for n in removal_index:
            after_cut[i].append(split_read[i][n])
    for i in range(0, len(split_read)):
        temp = len(after_cut[i])
        for n in range(0, temp):
            if n != 0:
                print(",", end='')
            split_line = after_cut[i][n]
            print(split_line.splitlines(), end='')
        print()


def paste(args):

    col = {}
    counter = 0
    for i in args[0:]:
        file = open(i)
        file_read = file.readlines()
        file.close()
        col[counter] = file_read
        counter = counter + 1
    max_list = 0
    for i in range(0, len(col)):
        if len(col[i]) > max_list:
            max_list = len(col[i])
    for i in range(0, max_list):
        for n in range(0, len(col)):
            if n != 0:
                 print(",", end='')
            if i in range(0, len(col[n])):
                text =col[n][i]
                print(text.splitlines(), end = '')
        print()
