def cat(args):
    """concatenate files and print on the standard output"""
    for arg in args:
        line = open(arg)
        text = line.read()
        print(text)
        line.close()
        print()


def tac(args):
    """concatenate and print files in reverse"""
    for arg in args:
        line = open(arg)
        trial = line.read().split('\n')
        for i in reversed(trial):
            print(i)
    line.close()
