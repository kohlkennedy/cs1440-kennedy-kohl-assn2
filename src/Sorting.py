def sort(args):
    """sort lines of text files"""
    for arg in args:
        with open(arg, 'r') as r:
            for line in sorted(r):
                print(line, end='')
    pass


def uniq(args):
    """report or omit repeated lines"""

    if args[0] == "-D":
        args.pop(0)
        sort(args)
    else:
        offset = 0
        countFlag = False
        uniqueArray = []
        if args[0] == "-c":
            offset = 1
            countFlag = True
        file = open(args[offset], 'r')
        for line in file:
            if line not in uniqueArray:
                uniqueArray.append(line)
        file.close()
        if countFlag:
            file = open(args[offset], 'r')
            file_read = file.readlines()
            for i in uniqueArray:
                print(file_read.count(i), end = '')
                print(" " + i)
        else:
            for i in uniqueArray:
                print(i, end = '')
