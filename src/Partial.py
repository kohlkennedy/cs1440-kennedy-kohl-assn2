def head(args):
    """output the first part of files"""
    offset = 0
    num_of_lines = 10
    if (len(args) < 1):
        print("Sorry, not enough arguments")
    else:
        if args[0] == "-n":
            num_of_lines = int(args[1])
            offset = 2
        file = open(args[0 + offset])
        file_read = file.readlines()
        for i in range(0, num_of_lines):
            if i in range(0, len(file_read)):
                print(file_read[i].split())


def tail(args):
    """output the last part of files"""
    offset = 0
    num_of_lines = 10
    if len(args) < 1:
        print("Sorry, not enough arguments")
    else:
        if args[0] == "-n":
            num_of_lines = int(args[1])
            offset = 2
        file = open(args[0 + offset])
        file_read = file.readlines()
        temp = []
        for i in range(0, num_of_lines):
            if (len(file_read) - 1 - i) in range(0, len(file_read)):
                temp.append(file_read[len(file_read) - i - 1])
        for i in range(0, len(temp)):
            print(temp[len(temp) - i - 1].splitlines())
