#!/usr/bin/env python

from Concatenate import cat, tac
from CutPaste import cut, paste
from Grep import grep
from Partial import head, tail
from Sorting import sort, uniq
from WordCount import wc
from Usage import usage

import sys



if len(sys.argv) < 2:
    usage()
    sys.exit(1)

tool = sys.argv[1]
argList = list()

for i in range(2, len(sys.argv)):
    argList.append(sys.argv[i])

if tool == "paste":
    paste(argList)
elif tool == "cut":
    cut(argList)
elif tool == "grep" :
    grep(argList)
elif tool == "head" :
    head(argList)
elif tool == "tail" :
    tail(argList)
elif tool == "wc" :
    wc(argList)
elif tool == "cat" :
    cat(argList)
elif tool == "tac" :
    tac(argList)
elif tool == "sort":
    sort(argList)
elif tool == "uniq":
    uniq(argList)
else:
    usage()
