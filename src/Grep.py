def grep(args):
    """print lines of files matching a pattern"""

    addedFlag = True
    offsetOfArguments = 0
    if args[0] == "-v":
        addedFlag = False
        offsetOfArguments = 1
    for i in args[1 + offsetOfArguments]:
        file = open(i, 'r')
        for line in file:
                if addedFlag:
                    if args[0] in line:
                        print(line, end='')
                else:
                    if args[0].lower() not in line.lower():
                        print(line, end='')
